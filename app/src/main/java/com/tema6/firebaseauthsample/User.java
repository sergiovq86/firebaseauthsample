package com.tema6.firebaseauthsample;

import java.util.Random;

/**
 * Created by Sergio on 30/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class User {

    private static final String[] NOMBRES = {"Carlos", "Ángel", "Ana", "Patricia", "Adrián", "Paula"};
    private static final String[] APELLIDOS = {"García", "López", "Martínez", "Ruíz", "Rodríguez", "Marín"};
    private static final String[] CITIES = {"Madrid", "Barcelona", "Sevilla", "Bilbao", "Valencia", "Málaga"};

    private String id;
    private String mail, name, surname, city;
    private int date;

    public User(String proveedor) {
        Random random = new Random();
        name = NOMBRES[random.nextInt(5)];
        surname = APELLIDOS[random.nextInt(5)];
        city = CITIES[random.nextInt(5)];
        mail = name.toLowerCase() + "." + surname.toLowerCase() + "@" + proveedor;
        date = 1960 + random.nextInt(50);

    }


    public User(String mail, String name, String surname, String city, int date) {
        this.mail = mail;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.date = date;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}

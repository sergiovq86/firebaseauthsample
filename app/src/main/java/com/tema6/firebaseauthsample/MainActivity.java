package com.tema6.firebaseauthsample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private MyAdapter adapter;
    private FirebaseFirestore db;
    private CollectionReference userCollection;
    private ArrayList<User> usuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser fUser = mAuth.getCurrentUser();

        String correo = fUser.getEmail();

        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("usuarios").document(correo).collection("misusuarios");

        configRecycler();
        leerDatos();
        botonesFunc();

    }


    private void configRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        adapter = new MyAdapter();

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    private void leerDatos() {
        /*userCollection.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                usuarios = new ArrayList<>();

                for (DocumentSnapshot document : queryDocumentSnapshots) {
                    HashMap<String, Object> user = (HashMap<String, Object>) document.getData();
                    String name = (String) user.get("name");
                    String surname = (String) user.get("surname");
                    String mail = (String) user.get("mail");
                    String city = (String) user.get("city");
                    long date = (Long) user.get("date");
                    User usuario = new User(mail, name, surname, city, (int) date);
                    usuario.setId(document.getId());
                    usuarios.add(usuario);
                }
                adapter.updateList(usuarios);
            }
        });*/

        /*userCollection.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                usuarios = new ArrayList<>();

                for (DocumentSnapshot document : queryDocumentSnapshots) {
                    User usuario = document.toObject(User.class);
                    usuario.setId(document.getId());
                    usuarios.add(usuario);
                }
                adapter.updateList(usuarios);
            }
        });*/

        userCollection.orderBy("date", Query.Direction.DESCENDING).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable QuerySnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                usuarios = new ArrayList<>();

                for (DocumentSnapshot document : value) {
                    User usuario = document.toObject(User.class);
                    usuario.setId(document.getId());
                    usuarios.add(usuario);
                }
                adapter.updateList(usuarios);
            }
        });
    }

    private void botonesFunc() {
        Button addHashMap = findViewById(R.id.buttonHashmap);
        addHashMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> user = new HashMap<>();
                user.put("name", "Sergio");
                user.put("surname", "Velasco");
                user.put("date", 1986);
                user.put("mail", "sergio.velasco@escuelaestech.es");
                user.put("city", "Linares");

                userCollection.add(user).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(MainActivity.this, "Añadido! ID: " + documentReference.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        Button addContacto = findViewById(R.id.buttonContacto);
        addContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user1 = new User("gmail.com");
                userCollection.add(user1).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(MainActivity.this, "Añadido! ID: " + documentReference.getId(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Button addId = findViewById(R.id.buttonId);
        addId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user1 = new User("gmail.com");
                userCollection.document(user1.getMail()).set(user1).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(MainActivity.this, "Id: " + user1.getMail(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Button update = findViewById(R.id.buttonUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userCollection.document("carlos.martínez@gmail.com").update("date", 2000)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                Toast.makeText(MainActivity.this, "Actualizado", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        Button deleteFirst = findViewById(R.id.buttonPrimero);
        deleteFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User firstUser = usuarios.get(0);
                userCollection.document(firstUser.getId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(MainActivity.this, "Eliminado", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        Button deleteAll = findViewById(R.id.buttonTodos);
        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (User user : usuarios) {
                    userCollection.document(user.getId()).delete();
                }
            }
        });
    }
}
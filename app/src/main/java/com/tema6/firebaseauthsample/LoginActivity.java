package com.tema6.firebaseauthsample;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * Created by Sergio on 25/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private TextView mStatusTextView;
    private TextInputEditText mEmailField;
    private TextInputEditText mPasswordField;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // instancia de FirebaseAuth
        mAuth = FirebaseAuth.getInstance();

        mStatusTextView = findViewById(R.id.status_text);
        mEmailField = findViewById(R.id.input_email);
        mPasswordField = findViewById(R.id.input_password);

        // boton login llama a doLogin
        Button mLoginButton = findViewById(R.id.btn_login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(mEmailField.getText().toString(), mPasswordField.getText().toString());
            }
        });

        // boton registro llama a createAccount
        Button mRegisterButton = findViewById(R.id.btn_register);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
            }
        });
    }

    @Override
    protected void onStart() {
        // on start es llamada inmediatamente despues a oncreate
        super.onStart();
        // con getCurrentUser obtenemos el ultimo usuario logueado en la app (si lo hay)
        FirebaseUser currentUser = mAuth.getCurrentUser();
        // si es nulo, es que no hay usuario logueado
        if (currentUser != null) {
            // acciones necesarias si el usuario ya está logueado, por ejemplo, abrir la siguiente actividad
            startMain();
        }
    }

    /**
     * Método para validar el email y la contraseña
     * @param email -> email escrito
     * @param password -> contraseña escrita
     * @return -> devuelve true si el email y la contraseña cumplen las condiciones
     */
    private boolean validateForm(String email, String password) {
        boolean valid = true;

        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    /**
     * método para hacer login en Firebase Authentication
     * @param email -> email del usuario
     * @param password -> contraseña del usuario
     */
    private void doLogin(String email, String password) {
        // si el email o la contraseña no son válidos, paramos el método
        if (!validateForm(email, password)) {
            return;
        }
        showProgress(true);

        //este método realiza el login con el usuario y contraseña enviados
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                showProgress(false);
                updateTextView();

                // si task isSuccessful es que la operacion ha ido bien
                if (task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    startMain();
                } else {
                    Toast.makeText(LoginActivity.this, "Login fallido", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Método para registrarse o crear una cuenta
     * @param email -> email del usuario
     * @param password -> contraseña del usuario
     */
    private void createAccount(String email, String password) {
        // si el email o la contraseña no son válidos, paramos el método
        if (!validateForm(email, password)) {
            return;
        }
        showProgress(true);

        // este método crea un usuario a partir de un email y un password
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                showProgress(false);
                updateTextView();

                //si la operación ha sido con éxito
                if (task.isSuccessful()) {
                    // UserProfileChangeRequest permite asignar datos adicionales al usuario creado
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName("Sergio").build();

                    // si FirebaseUser no es nulo, actualizamos el usuario con los datos adicionales
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null)
                        user.updateProfile(profileUpdates);

                    // manda un email a la cuenta registrada para verificar el correo
                    sendEmail();
                } else {
                    Toast.makeText(LoginActivity.this, "Fallo al registrar al usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Método para mandar un email de verificación
     */
    private void sendEmail() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            // sendEmailVerification manda el email de verificación al usuario que llama al método
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    // si task isSuccessful, se ha enviado el correo correctamente
                    if (task.isSuccessful()) {
                        Toast.makeText(LoginActivity.this,
                                "Email de verificación enviado a " + user.getEmail(),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this,
                                "Fallo al enviar el email de verificación", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    /**
     * Método que oculta/muestra el símbolo de carga
     * @param show -> true para mostrar carga, false para ocultar
     */
    private void showProgress(boolean show) {
        ProgressBar progressBar = findViewById(R.id.progressBar);
        ImageView logo = findViewById(R.id.logo);

        if (show) {
            progressBar.setVisibility(View.VISIBLE);
            logo.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            logo.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Método para iniciar MainActivity
     */
    private void startMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Método para actualizar el TextView con la última información del usuario
     */
    private void updateTextView() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            mStatusTextView.setText("User logged in\n" + user.getEmail() + "\n" + user.isEmailVerified() + "\n" + user.getUid());
        } else {
            mStatusTextView.setText("User logged out");
        }
    }
}

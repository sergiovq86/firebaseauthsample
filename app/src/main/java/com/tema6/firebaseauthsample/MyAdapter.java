package com.tema6.firebaseauthsample;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by Sergio on 30/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

    private final ArrayList<User> listado;

    public MyAdapter() {
        listado = new ArrayList<>();
    }

    @NonNull
    @NotNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_holder, parent, false);

        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MyAdapter.MyHolder holder, int position) {
        User user = listado.get(position);
        holder.tvName.setText(user.getName());
        holder.tvCity.setText(user.getCity());
        holder.tvDate.setText(String.valueOf(user.getDate()));
        holder.tvEmail.setText(user.getMail());
        holder.tvSurname.setText(user.getSurname());
    }

    @Override
    public int getItemCount() {
        return listado.size();
    }

    public void updateList(ArrayList<User> list) {
        listado.clear();
        listado.addAll(list);
        notifyDataSetChanged();
    }

    static class MyHolder extends RecyclerView.ViewHolder {

        private final TextView tvName;
        private final TextView tvSurname;
        private final TextView tvEmail;
        private final TextView tvDate;
        private final TextView tvCity;

        public MyHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            tvSurname = itemView.findViewById(R.id.tvSurname);
            tvEmail = itemView.findViewById(R.id.tvCorreo);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvCity = itemView.findViewById(R.id.tvCity);
        }
    }
}
